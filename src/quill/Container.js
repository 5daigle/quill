import React, {useState} from 'react'
//Quill
import ReactQuill from 'react-quill'
import modules from './modules'
import formats from './formats'
//Css
import 'react-quill/dist/quill.snow.css'
import "katex/dist/katex.min.css"
import "highlight.js/styles/monokai-sublime.css"

export default function Container() {

    const defaultDelta = {
        ops: [{ insert: "" }]
      }

    const [value, setValue] = useState(defaultDelta)

     // onChange expects a function with these 4 arguments
    function handleChange(content, delta, source, editor) {
        setValue(editor.getContents());
    }

    return <div>
        <ReactQuill 
            theme="snow"
            modules={modules}
            formats={formats}
            value={value}
            onChange={handleChange}
        />
    </div>
}
