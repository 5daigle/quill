
/*
* Quill editor formats
* See https://quilljs.com/docs/formats/
*/
const formats = [

    //inline
    'background',
    'bold',
    'code',
    'color',
    'font',
    'italic',
    'link',
    //'size',
    'strike',
    'underline',

    //Block
    'align',
        //'blockquote',
    'bullet',
    'code-block',
    'header',
    'indent',
    'list',

    //Embed
    'formula'
        //'image',
        //'video',
]

export default formats