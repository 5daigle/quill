import hljs from 'highlight.js'

//Configure hljs for code language to style
hljs.configure({
    languages: ['javascript', 'ruby', 'python']
})

const modules = {
    //The toolbar buttons are in the same order than for google doc
    toolbar: [
        //Headers : H1, H2, Normal
        [ {'header':["1", "2", false]} ],
        //Font
        [ { 'font': [] } ],
        //Size: small, normal, large, (not used: huge)
        //[{ 'size': ['small', false, 'large'] }],
        //Basic styling
        [
            'bold', 
            'italic', 
            'underline',
            'strike',
            'code'
        ],
        //Colors
        [
            { 'color':[] }, 
            { 'background': []}
        ],
        //Embed (not used: image, video)
        [
            'link', 
            'formula'
        ],
        //Align
        [
            { 'align': []}
        ],
        //Lists
        [
            { 'list': 'ordered'}, 
            { 'list': 'bullet' }
        ],
        //Block
        [
            //'blockquote', 
            'code-block'
        ],
        //Indent - Outdent
        [
            { 'indent': '-1'}, 
            { 'indent': '+1' }
        ],
        //Clean styles
        ['clean'],
    ],
    clipboard: {
      // toggle to add extra line breaks when pasting HTML:
      matchVisual: false,
    },
    //Enable code block style
    syntax: {
        highlight: text => hljs.highlightAuto(text).value
    },
}

export default modules